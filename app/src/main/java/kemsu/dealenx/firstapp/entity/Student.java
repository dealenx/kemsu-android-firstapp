package kemsu.dealenx.firstapp.entity;

public class Student {

    public Student(long id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public long id;

    public String name;

    public String address;
}
