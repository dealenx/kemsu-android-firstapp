package kemsu.dealenx.firstapp.main;

import android.util.Log;
import java.util.ArrayList;
import android.widget.Button;
import android.os.Handler;
import kemsu.dealenx.firstapp.R;

public class TrafficLight implements Runnable {

        private final static int INTERVAL = 5000;

        private Handler mHandler;

        private int stateLight = 0;

        private static final String TAG = "TrafficLight";

        public ArrayList<Button> buttons;

        public TrafficLight(ArrayList<Button> buttons) {
                mHandler = new Handler();
                this.buttons = buttons;
        }

        public void start() {
                run();
        }

        @Override
        public void run() {
                // put here the logic that you want to be executed
                funcLoop();

                mHandler.postDelayed(this, INTERVAL);
        }

        public void stop() {
                mHandler.removeCallbacks(this);
        }

        public void funcLoop() {
                Log.i(TAG, "event loop hello");

                switch (stateLight) {
                        case 0:  switchRed();
                                break;
                        case 1: switchYellow();
                                break;
                        case 2: switchGreen();
                                break;
                        default: Log.i(TAG, "stateLight > 2");
                                break;
                }

                this.stateLight++;
                if(this.stateLight == 3) {
                        this.stateLight  = 0;
                }

        }


        public int state = 0;

        public void switchRed() {
                Log.i(TAG, "is red");
                buttons.get(0).setBackgroundResource(R.drawable.circle_red);
                buttons.get(1).setBackgroundResource(R.drawable.circle);
                buttons.get(2).setBackgroundResource(R.drawable.circle);
        }

        public void switchYellow() {
                Log.i(TAG, "is yellow");
                buttons.get(0).setBackgroundResource(R.drawable.circle);
                buttons.get(1).setBackgroundResource(R.drawable.circle_yellow);
                buttons.get(2).setBackgroundResource(R.drawable.circle);
        }

        public void switchGreen() {
                Log.i(TAG, "is green");
                buttons.get(0).setBackgroundResource(R.drawable.circle);
                buttons.get(1).setBackgroundResource(R.drawable.circle);
                buttons.get(2).setBackgroundResource(R.drawable.circle_green);
        }

}
