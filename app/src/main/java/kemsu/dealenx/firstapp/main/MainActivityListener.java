package kemsu.dealenx.firstapp.main;

import android.util.Log;

public class MainActivityListener {
    private static final String TAG = "MainActivityListener";

    public void getTest() {
        Log.i(TAG, "MainActivityListener.getTest");
    }

    /*public MainActivityListener(Context context, Callback callback) {
        // ...
    }*/

    public MainActivityListener() {
        Log.i(TAG, "created MainActivityListener()");
    }

    void start() {
        // connect to system location service
    }

    void stop() {
        // disconnect from system location service
    }

}
