package kemsu.dealenx.firstapp.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import java.util.ArrayList;
import kemsu.dealenx.firstapp.R;
import android.widget.Button;
import android.os.AsyncTask;



public class MainActivity extends AppCompatActivity {

    private MainActivityListener mainActivityListener = new MainActivityListener();

    private static final String TAG = "MyActivity";

    private static TrafficLight trafficLight;

    private ArrayList<Button> buttons;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inittrafficLight();

    }

    @Override
    public void onStart() {
        super.onStart();

        trafficLight.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        this.mainActivityListener.getTest();

        trafficLight.stop();
    }

    private void inittrafficLight() {
        buttons = new ArrayList<Button>();

        buttons.add((Button) findViewById(R.id.Button0));
        buttons.add((Button) findViewById(R.id.Button1));
        buttons.add((Button) findViewById(R.id.Button2));

        trafficLight  = new TrafficLight(buttons);
    }

}
